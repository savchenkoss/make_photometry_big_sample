#!/usr/bin/env python

import time
import numpy
from astropy.table import Table
import requests
import urllib.request
from PIL import Image
from io import BytesIO
import math
import argparse


def getimages(ra, dec, size=240, filters="grizy"):
    """Query ps1filenames.py service to get a list of images
    ra, dec = position in degrees
    size = image size in pixels (0.25 arcsec/pixel)
    filters = string with filters to include
    Returns a table with the results
    """
    service = "https://ps1images.stsci.edu/cgi-bin/ps1filenames.py"
    url = ("{service}?ra={ra}&dec={dec}&size={size}&format=fits"
           "&filters={filters}").format(**locals())
    table = Table.read(url, format='ascii')
    return table


def geturl(ra, dec, size=240, output_size=None, filters="grizy", format="jpg", color=False):
    """Get URL for images in the table
    ra, dec = position in degrees
    size = extracted image size in pixels (0.25 arcsec/pixel)
    output_size = output (display) image size in pixels (default = size).
                  output_size has no effect for fits format images.
    filters = string with filters to include
    format = data format (options are "jpg", "png" or "fits")
    color = if True, creates a color image (only for jpg or png format).
            Default is return a list of URLs for single-filter grayscale images.
    Returns a string with the URL
    """

    if (color is True) and (format == "fits"):
        raise ValueError("color images are available only for jpg or png formats")
    if format not in ("jpg", "png", "fits"):
        raise ValueError("format must be one of jpg, png, fits")
    table = getimages(ra, dec, size=size, filters=filters)
    url = ("https://ps1images.stsci.edu/cgi-bin/fitscut.cgi?"
           "ra={ra}&dec={dec}&size={size}&format={format}").format(**locals())
    if output_size:
        url = url + "&output_size={}".format(output_size)
    # sort filters from red to blue
    flist = ["yzirg".find(x) for x in table['filter']]
    table = table[numpy.argsort(flist)]
    if color:
        if len(table) > 3:
            # pick 3 filters
            table = table[[0, len(table)//2, len(table)-1]]
        for i, param in enumerate(["red", "green", "blue"]):
            url = url + "&{}={}".format(param, table['filename'][i])
    else:
        urlbase = url + "&red="
        url = []
        for filename in table['filename']:
            url.append(urlbase+filename)
    return url


def getcolorim(ra, dec, size=240, output_size=None, filters="grizy", format="jpg"):
    """Get color image at a sky position
    ra, dec = position in degrees
    size = extracted image size in pixels (0.25 arcsec/pixel)
    output_size = output (display) image size in pixels (default = size).
                  output_size has no effect for fits format images.
    filters = string with filters to include
    format = data format (options are "jpg", "png")
    Returns the image
    """
    if format not in ("jpg", "png"):
        raise ValueError("format must be jpg or png")
    url = geturl(ra, dec, size=size, filters=filters, output_size=output_size,
                 format=format, color=True)
    r = requests.get(url)
    im = Image.open(BytesIO(r.content))
    return im


def getgrayim(ra, dec, size=240, output_size=None, filter="g", format="jpg"):
    """Get grayscale image at a sky position
    ra, dec = position in degrees
    size = extracted image size in pixels (0.25 arcsec/pixel)
    output_size = output (display) image size in pixels (default = size).
                  output_size has no effect for fits format images.
    filter = string with filter to extract (one of grizy)
    format = data format (options are "jpg", "png")
    Returns the image
    """

    if format not in ("jpg", "png"):
        raise ValueError("format must be jpg or png")
    if filter not in list("grizy"):
        raise ValueError("filter must be one of grizy")
    url = geturl(ra, dec, size=size, filters=filter, output_size=output_size, format=format)
    r = requests.get(url[0])
    im = Image.open(BytesIO(r.content))
    return im


def main(ra, dec, output_image='image.fits', size=1260, factor=1., band="i", format="fits"):
    size = int(size)
    output_size = int(math.floor(size/factor))
    new_scale = 0.25*float(size)/float(output_size)
    # print('New scale is %.3f arcsec/pix' % (new_scale))

    # grayscale image
    # gim = getgrayim(ra,dec,size=size,filter="g", output_size=1260)
    # gim.save('out.jpeg')

    if format == 'fits':
        # fits image
        for i in range(10):
            try:
                fitsurl = geturl(ra, dec, size=size, filters=band, format=format, output_size=output_size)
                break
            except:
                time.sleep(10)
        else:
            exit(0)
        for i in range(10):
            try:
                urllib.request.urlretrieve(fitsurl[0], output_image)
                break
            except:
                time.sleep(4)
        else:
            exit(0)
        # fh = fits.open(fitsurl[0])
        # data = fh[0].data
        # header = fh[0].header
        # outHDU = fits.PrimaryHDU(data, header=header)
        # outHDU.writeto(output_image, clobber=True)
    else:
        gim = getcolorim(ra, dec, size=size, output_size=output_size, filters="grizy", format="jpg")
        if '.fits' in output_image:
            gim.save(output_image.split('.fits')[0]+'.jpg')
        else:
            gim.save(output_image)
    return new_scale


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Get an image from Pan-STARRS")
    parser.add_argument("ra", help="Right ascension [deg]")
    parser.add_argument("dec", help="Declination [deg]")

    parser.add_argument("--outputImage", nargs='?', const=1, type=str, default='image.fits',
                        help="Optional: Output image")

    parser.add_argument("--size", nargs='?', const=1, type=int, default=1280,
                        help="Optional: extracted image size in pixels (0.25 arcsec/pixel)")
    parser.add_argument("--factor", nargs='?', const=1, type=float, default=1.,
                        help="Optional: Rebinning factor (should be >=1)")
    parser.add_argument("--band", nargs='?', const=1, type=str, default="i",
                        help="Optional: band")
    parser.add_argument("--format", nargs='?', const=1, type=str, default="fits",
                        help="Optional: format: fits or jpg")

    args = parser.parse_args()
    ra = args.ra
    dec = args.dec

    output_image = args.outputImage
    size = args.size
    factor = args.factor
    band = args.band
    format = args.format

    main(ra, dec, output_image=output_image, size=size, factor=factor, band=band, format=format)


'''
ra = 0.183
dec = 7.090
size = 1280
factor = 5.
main(ra, dec, output_image='image.fits', size=size, factor=factor, band="i", format="fits")
'''
