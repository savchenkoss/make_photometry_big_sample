#! /usr/bin/env python

import argparse
import os
import shutil
import subprocess
from pathlib import Path
import json
import threading
import time
import paramiko
import numpy as np
from astropy.io import fits
from astropy.wcs import WCS
import astropy.units as units
import multiprocessing
from astroquery.gaia import Gaia
import socket
from libs import retrieve_panstarrs_image
from libs import se


def http_proxy_tunnel_connect(proxy, target, timeout=None):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(timeout)
    sock.connect(proxy)
    cmd_connect = "CONNECT %s:%d HTTP/1.1\r\n\r\n" % target
    sock.sendall(cmd_connect.encode())
    response = []
    sock.settimeout(2)  # quick hack - replace this with something better performing.
    while True:
        chunk = sock.recv(1024).decode()
        if not chunk:  # if something goes wrong
            break
        response.append(chunk)
        if "\r\n\r\n" in chunk:  # we do not want to read too far ;)
            break
    response = ''.join(response)
    return sock


class Comm:
    def __init__(self, proxy=None, port=None):
        self.proxy = proxy
        self.port = port
        self.connected = False

    def connect(self):
        # Make SSH connection
        self.ssh_client = paramiko.SSHClient()
        self.ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        if self.proxy is not None:
            sock = http_proxy_tunnel_connect(proxy=(self.proxy, self.port),
                                             target=("observ.astro.spbu.ru", 22),
                                             timeout=50)
            self.ssh_client.connect(hostname='observ.astro.spbu.ru', sock=sock, username='panneur', password="")
        else:
            self.ssh_client.connect(hostname='observ.astro.spbu.ru', sock=None,
                                    username='panneur', password="")
        self.ftp_client = self.ssh_client.open_sftp()
        self.connected = True

    def disconnect(self):
        self.ftp_client.close()
        self.ssh_client.close()
        self.connected = False

    def get_next(self, testing=False):
        # self.connect()
        print("Asking a remote server for the next object")
        while 1:
            if testing is False:
                command = f"python3 dispenser/dispenser.py ~/dispenser/jobs/photometry_big_sample {os.uname().nodename}"
            else:
                command = "python3 dispenser/dispenser.py "
                command += f"~/dispenser/jobs/photometry_big_sample {os.uname().nodename} --test"
            stdin, stdout, stderr = self.ssh_client.exec_command(command)
            for butes in stdout.read().splitlines():
                line = butes.decode("utf-8")
                print(line)
                if "Empty" in line:
                    print("All done")
                    open("empty", "w").close()
                    exit(10)
                if "Busy" in line:
                    print("Server is busy. Waiting for 5 seconds.")
                    time.sleep(5)
                    continue
                self.name = line.split(",")[0]
                self.ra = float(line.split(",")[1])
                self.dec = float(line.split(",")[2])
                self.size = float(line.split(",")[3])
                print(f"Next object: {self.name}")
                return self.name, self.ra, self.dec, self.size

    def put_dir(self, source, target):
        self.mkdir(str(target), ignore_existing=True)
        for item in os.listdir(source):
            if os.path.isfile(os.path.join(source, item)):
                self.ftp_client.put(os.path.join(source, item), f'{target}/{item}')
            else:
                self.mkdir(f'{target}/{item}', ignore_existing=True)
                self.put_dir(os.path.join(source, item), f'{target}/{item}')

    def mkdir(self, path, mode=511, ignore_existing=False):
        try:
            self.ftp_client.mkdir(path, mode)
        except IOError:
            if ignore_existing:
                pass
            else:
                raise

    def upload(self, local_file_name, remote_file_name):
        self.ftp_client.put(local_file_name, remote_file_name)

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.connected:
            self.disconnect()


def download_multiband(ra, dec, output_image, size, factor, bands):
    threads = []
    for band in bands:
        out_name = output_image[:-5] + f"_{band}.fits"
        t = threading.Thread(target=retrieve_panstarrs_image.main,
                             args=(ra, dec, out_name, size, factor, band, "fits"))
        threads.append(t)
    for t in threads:
        t.start()
    for t in threads:
        t.join()


def call_se(image_name, sigma=None):
    callString = f"sex {image_name} -c ./libs/default.sex "
    callString += "-DEBLEND_NTHRESH 32 -DEBLEND_MINCONT 0.04 "
    callString += "-CHECKIMAGE_TYPE  SEGMENTATION "
    callString += "-CHECKIMAGE_NAME  segm.fits"
    if sigma is not None:
        callString = callString + f" -DETECT_THRESH {sigma} -ANALYSIS_THRESH {sigma} -DETECT_MINAREA 25 "
    header = fits.getheader(image_name)
    if "EXPTIME" in header:
        exptime = float(header['EXPTIME'])
        mag_zpt = 25 + 2.5 * np.log10(exptime)
        gain = float(header['CELL.GAIN'])
        saturation = float(header['CELL.SATURATION'])
        callString += f" -MAG_ZEROPOINT {mag_zpt} -GAIN {gain} -SATUR_LEVEL {saturation} -PIXEL_SCALE 0.25 "
    subprocess.call(callString, shell=True)
    catalogue = se.SExCatalogue("field.cat")
    return catalogue


def run_gaia(coordinate, radius, res_queue):
    while 1:
        try:
            job = Gaia.cone_search(coordinate=coordinate, radius=radius)
            break
        except:
            pass
    res_queue.put(job.get_results())


def get_gaia_table(sky_cen, width):
    while 1:
        try:
            res_queue = multiprocessing.Queue()
            p = multiprocessing.Process(target=run_gaia, args=(sky_cen, width, res_queue))
            p.start()
            p.join(10)
            try:
                results = res_queue.get(1)
            except:
                print("Gaia responce hanged. Let's kill it and try again.")
                # Process hanged for far too long, let's kill it and try again
                p.terminate()
                p.join()
                time.sleep(3)
                continue
            break
        except ConnectionError:
            print("Can't connect to GAIA server. Trying again in 3 seconds.")
            time.sleep(3)
    ra_list = []
    dec_list = []
    g_mag_list = []
    for i in range(len(results)):
        ra = results[i]["ra"]
        dec = results[i]["dec"]
        rp_excess = results[i]["phot_bp_rp_excess_factor"]
        if rp_excess > 5:
            continue
        g_mag = results[i]["phot_g_mean_mag"]
        ra_list.append(ra)
        dec_list.append(dec)
        g_mag_list.append(g_mag)
    return ra_list, dec_list, g_mag_list


def make_gaia_mask(image_name):
    data = fits.getdata(image_name)
    mask_data = np.zeros_like(data)
    header = fits.getheader(image_name)
    image_wcs = WCS(header)
    y_size, x_size = data.shape
    x_cen = x_size / 2
    y_cen = y_size / 2
    sky_cen = image_wcs.pixel_to_world(x_cen, y_cen)
    width = units.degree * 0.25 * x_size / 3600
    ra_list, dec_list, g_mag_list = get_gaia_table(sky_cen, width)
    for ra, dec, g_mag in zip(ra_list, dec_list, g_mag_list):
        r_lim = 55 * np.exp(-0.2385*(g_mag-11.64))
        x_star, y_star = image_wcs.world_to_pixel_values(ra, dec)
        if np.hypot(x_star-x_cen, y_star-y_cen) < r_lim:
            continue
        for x in range(int(x_star-r_lim), int(x_star+r_lim+1)):
            for y in range(int(y_star-r_lim), int(y_star+r_lim+1)):
                if (x < 0) or (y < 0) or (x >= x_size) or (y >= y_size):
                    continue
                if np.hypot(x_star-x, y_star-y) < r_lim:
                    mask_data[y, x] = 1
    return mask_data


def main(args):
    remote_path = Path("/home/panneur/photometry_big_sample")
    retriever = Comm(args.proxy, args.port)
    retriever.connect()
    workdir = Path("workdir")

    par_list = []
    for line in open("libs/default.param"):
        par_list.append(line.strip())

    while 1:
        if Path("/tmp/stop_photometry").exists():
            exit()
        if workdir.exists():
            shutil.rmtree(workdir)
        os.makedirs(workdir)
        name, ra, dec, size = retriever.get_next()

        # Download fits images
        download_multiband(ra, dec, str(workdir/"image.fits"), 10*size, 1.0, "rgiyz")

        gaia_mask = make_gaia_mask(workdir / "image_r.fits")

        for band in "rgiyz":
            hdu = fits.open(workdir / f"image_{band}.fits", mode="update")
            hdu[0].data[gaia_mask > 0] = np.nan
            hdu.flush()

        # Let's call SExtrator for a combined gri file to find the galaxy location
        data_g = fits.getdata(workdir / "image_g.fits")
        data_r = fits.getdata(workdir / "image_r.fits")
        data_i = fits.getdata(workdir / "image_i.fits")
        data_combined = np.nanmean([data_g, data_r, data_i], axis=0)
        fits.PrimaryHDU(data=data_combined).writeto(workdir / "image_comb.fits", overwrite=True)

        sigma = None
        print("SEx: combined image")
        catalogue = call_se(workdir / "image_comb.fits")
        # Remove objects that appear to be not galaxies
        catalogue.remove_lower("ISOAREA_IMAGE", 15)  # Too low area

        if len(catalogue) == 0:
            # Try deeper
            print("SEx: deeper combined image")
            sigma = 2
            catalogue = call_se(workdir / "image_comb.fits", sigma=2)
            # Remove objects that appear to be not galaxies
            catalogue.remove_lower("ISOAREA_IMAGE", 15)  # Too low area

        if len(catalogue) == 0:
            print("SEx: even deeper combined image")
            # Try even deeper
            sigma = 1
            catalogue = call_se(workdir / "image_comb.fits", sigma=1)
            # Remove objects that appear to be not galaxies
            catalogue.remove_lower("ISOAREA_IMAGE", 15)  # Too low area

        if len(catalogue) == 0:
            print("No objects on the deeper image")
            # We are really deep now, but still no object detected. Let's just write NaNs to a
            # json files and exit
            for band in "rgiyz":
                fout = workdir / f"{name}_{band}.json"
                nan_obj = {name: np.nan for name in par_list}
                with open(fout, "w") as fout:
                    json.dump(nan_obj, fout)
                    retriever.upload(str(workdir / f"{name}_{band}.json"),
                                     str(remote_path / f"{name}_{band}.json"))
            continue

        # Find the galaxy coordinates
        header = fits.getheader(workdir / "image_comb.fits")
        size_x = float(header["NAXIS1"])
        size_y = float(header["NAXIS2"])
        obj = catalogue.find_nearest(size_x/2, size_y/2)
        x_cen_gal = obj["X_IMAGE"]
        y_cen_gal = obj["Y_IMAGE"]
        print(f"{x_cen_gal=}, {y_cen_gal=}")
        semi_axis = obj["A_IMAGE"] * obj["KRON_RADIUS"]
        print(f"{semi_axis=}")

        for band in "rgiyz":
            # Call sextractor
            print(f"Workding with {band} band")
            fout = workdir / f"{name}_{band}.json"
            cand_file = workdir / f"image_{band}.fits"

            for sigma in (3, 2, 1):
                catalogue = call_se(cand_file, sigma=sigma)
                if len(catalogue) == 0:
                    print("   Deeper (empty)")
                    continue
                obj = catalogue.find_nearest(x_cen_gal, y_cen_gal)
                dist = np.hypot(x_cen_gal - obj["X_IMAGE"], y_cen_gal - obj["Y_IMAGE"])
                print(dist)
                if dist > semi_axis:
                    print("   Deeper (too far away)")
                    # Too far from the reference
                    continue
                print("   OK")
                with open(fout, "w") as fout:
                    json.dump(obj, fout)
                break
            else:
                print("   Only nans")
                nan_obj = {name: np.nan for name in par_list}
                with open(fout, "w") as fout:
                    json.dump(nan_obj, fout)
                continue
            retriever.upload(str(workdir / f"{name}_{band}.json"), str(remote_path / f"{name}_{band}.json"))



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--proxy", help="Proxy address, if needed.")
    parser.add_argument("--port", help="Proxy port, if needed.", type=int, default=None)
    args = parser.parse_args()
    main(args)
